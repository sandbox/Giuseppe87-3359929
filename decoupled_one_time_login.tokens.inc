<?php


/**
 * Implements hook_token_info().
 */
function decoupled_one_time_login_token_info() {
  $user['decoupled-one-time-login'] = [
    'name' => t('One time login token'),
    'description' => t('A one time login token'),
    'restricted' => TRUE,
  ];

  return [
    'tokens' => ['user' => $user],
  ];
}
