<?php

namespace Drupal\decoupled_one_time_login;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\user\UserInterface;

class DecoupledOneTimeLoginTokens implements DecoupledOneTimeLoginTokensInterface {

  protected TimeInterface $time;

  public function __construct(TimeInterface $time) {
    $this->time = $time;
  }

  public function oneTimeLogin(UserInterface $account): string {
    $timestamp = $this->time->getRequestTime();
    $hash = user_pass_rehash($account, $timestamp);
    return "$timestamp/$hash";
  }

}
