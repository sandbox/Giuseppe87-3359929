<?php

namespace Drupal\decoupled_one_time_login;


use Drupal\user\UserInterface;

interface DecoupledOneTimeLoginTokensInterface {

  /**
   * @see user_pass_reset_url()
   */
  public function oneTimeLogin(UserInterface $account): string;

}
