<?php

namespace Drupal\decoupled_one_time_login;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountInterface;

class DecoupledOneTimeLoginMailer implements DecoupledOneTimeLoginMailerInterface {

  protected EntityTypeManagerInterface $entityTypeManager;

  protected MailManagerInterface $mailManager;

  protected ConfigFactoryInterface $configFactory;

  public function __construct(EntityTypeManagerInterface $entity_type_manager, MailManagerInterface $mail_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->mailManager = $mail_manager;
    $this->configFactory = $config_factory;
  }

  function sendMail(string $op = 'password_reset', AccountInterface $account = NULL, string $langcode = NULL) {
    $params['account'] = $account;
    $langcode = $langcode ?: $account->getPreferredLangcode();
    $config = $this->configFactory->get('system.site');

    $site_mail = $config->get('mail_notification');

    if (empty($site_mail)) {
      $site_mail = $config->get('mail');
    }
    if (empty($site_mail)) {
      $site_mail = ini_get('sendmail_from');
    }

    $mail = $this->mailManager->mail('decoupled_one_time_login', $op, $account->getEmail(), $langcode, $params, $site_mail);

    return empty($mail) ? NULL : $mail['result'];
  }

}
