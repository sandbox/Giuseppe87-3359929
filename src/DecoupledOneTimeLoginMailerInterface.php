<?php

namespace Drupal\decoupled_one_time_login;


use Drupal\Core\Session\AccountInterface;

interface DecoupledOneTimeLoginMailerInterface {

  function sendMail(string $op = 'password_reset', AccountInterface $account = NULL, string $langcode = NULL);

}
