<?php

namespace Drupal\decoupled_one_time_login\Plugin\EmailBuilder;

use Drupal\symfony_mailer\EmailFactoryInterface;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\MailerHelperTrait;
use Drupal\symfony_mailer\Processor\EmailBuilderBase;
use Drupal\symfony_mailer\Processor\TokenProcessorTrait;
use Drupal\user\UserInterface;

/**
 *
 * @EmailBuilder(
 *   id = "decoupled_one_time_login",
 *   sub_types = {
 *     "password_reset" = @Translation("Decoupled password mail")
 *   },
 *   proxy = TRUE,
 *   common_adjusters = {"email_subject", "email_body", "email_skip_sending"},
 * )
 *
 */
class PasswordResetBuilder extends EmailBuilderBase {

  use MailerHelperTrait;
  use TokenProcessorTrait;

  /**
   * Saves the parameters for a newly created email.
   *
   * @param \Drupal\symfony_mailer\EmailInterface $email
   *   The email to modify.
   * @param \Drupal\user\UserInterface $user
   *   The user.
   */
  public function createParams(EmailInterface $email, UserInterface $user = NULL) {
    assert($user != NULL);
    $email->setParam('user', $user);
  }

  /**
   * {@inheritdoc}
   */
  public function fromArray(EmailFactoryInterface $factory, array $message) {
    return $factory->newTypedEmail($message['module'], $message['key'], $message['params']['account']);
  }

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email) {
    $email->setTo($email->getParam('user'));
    $this->tokenOptions([
      'callback' => '_decoupled_one_time_login_mail_tokens',
      'clear' => TRUE,
    ]);
  }

}
